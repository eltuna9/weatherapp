/*!
 
 */

 $(document).ready(function($){
	 var generalOptions = {
		 serverUrl : "http://localhost:8080/weather?"
	 }
	$("#btnVer").on("click", function(){
		getWheather($("#txtCity").val(),$("#cmbUnit").val());
	});	 
	$(".wheather,#mapa").hide();
	function getWheather(city,units){
		var wheatherUrl = generalOptions.serverUrl+"city="+city+"&units="+units;
		doGetRequest(wheatherUrl,showWheatherAndMap, showError)
	}

	function showWheatherAndMap(data){
		showWheather(data);
		showMap(data);
	}

	function showWheather(data){
		$("#cityName").text(data.name);
		$("#temp").text(data.main.temp+ $("#cmbUnit option:selected").text());
		$("#state").text(data.weather[0].description);
		$(".wheather").show();		
	}

	function showError(){
		alert("Se ha producido un error al realizar la consulta.");
	}

	function showMap(data){
		$("#mapa").show();
		map = new google.maps.Map(document.getElementById('mapa'), {
		center: {lat: data.coord.lat, lng: data.coord.lon},
		zoom: 12
		});
		
	}
	
	function doGetRequest(getUrl,successFunction,errorFunction){
        	$.ajax({
            url : getUrl,
            type : 'GET',                
            contentType:'application/json',
            success : function(data) { 
            	successFunction(data);            	  	                  
            },
            error : function(xhr, textStatus, errorThrown)
            {
                console.log("Request: "+JSON.stringify(xhr));
                if(errorFunction != undefined)
                	errorFunction();
            }
         });
	}
		
 })