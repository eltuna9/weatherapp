package weatherApp;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

@RestController
public class CityWheatherController {
   
	private static final String API_BASIC_URL = "http://api.openweathermap.org/data/2.5/weather?q=";
    private static final String UNITS = "&units=";
    private static final String LANG = "&lang=es"; 
    private static final String API_KEY = "&APPID=ca147896cad69f2c6e786ee7177c9882";  

    @CrossOrigin(origins = "*")
	@RequestMapping("/weather")
    public Object cityWeather(@RequestParam(value="city", defaultValue="Córdoba") String city,@RequestParam(value="units", defaultValue="metric") String units ) {
    	try {
			RestTemplate restTemplate = new RestTemplate();
			CityWeather cityWeather = restTemplate.getForObject(API_BASIC_URL+city+UNITS+units+LANG+API_KEY, CityWeather.class);
			return cityWeather;
		} catch (RestClientException e) {			
			e.printStackTrace();
			return "Ha ocurrido un error y no ha sido posible obtener el clima";
		}
    }
}
