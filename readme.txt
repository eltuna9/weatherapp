La soluci�n que plante� es usar una aplicacion Spring RESTful que se comunique con la API del servicio del clima. Y por otro lado un cliente web que consume esa aplicaci�n a trav�s de AJAX. 
La visualizaci�n del mapa se procesa completamente del lado del cliente.
El archivo que procesa las llamadas AJAX y el comportamiento de la UI es functions.js que esta en la carpeta Vendor (WebUI).
Para el dise�o de la UI, use Bootstrap 3.

**Al ejecutar la aplicacion desde el IDE, se empaqueta en un JAR, que aprovecha la capacidad de Spring de usar Tomcat embebido y permitir su ejecucion en el puerto 8080, por lo que ese puerto debe estar libre a la hora de ejecutar el JAR. 